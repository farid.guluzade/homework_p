let tabs = document.querySelector('.tabs')
let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelector('.tabs-content')


console.log(tabsTitle);
for (let item of tabsTitle) {
    item.addEventListener('click', () => {
        document.querySelector('.active').classList.remove('active');
        item.classList.add('active');
        for (let n = 0; n < tabsContent.children.length; n++) {
            if (tabs.children[n].classList.contains('active')) {
                document.querySelector('.active-tab').classList.remove('active-tab');
                tabsContent.children[n].classList.add('active-tab');
            }
        }
    })
}