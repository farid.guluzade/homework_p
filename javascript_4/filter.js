function filterBy(n, dataType) {
    let newArray = n.filter(item => typeof(item) != typeof(dataType));

    return newArray;
}

let n = ['hello', 'world', 23, '23', null];

let newArray = filterBy(n, 'string');

for (let m of newArray) {
    console.log(m);

}