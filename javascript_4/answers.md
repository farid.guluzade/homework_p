arr.forEach(function callback(currentValue, index, array) {
    //your iterator
}[, thisArg]);

Options:

callback
The function to be called for each element of the array. It takes from one to three arguments

currentValue
Current element being processed in an array.

Optional

Index of the currently processed element in an array

agaUnecessary
The array on which the passage is made

thisArg
Optional parameter. The value used as this when calling the callback function.
