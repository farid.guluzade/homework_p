function NameChecker() {
    let Name = "";
    while (Name == "") {
        Name = prompt("Please, enter username:", "");
    }
    return Name;
}

function AgeChecker() {
    let a = prompt("Please, enter your age", "");
    if (isNaN(a)) return AgeChecker();
    else return a;
}

let name = NameChecker();
let age = AgeChecker();

if (age < 18) alert("You are not allowed to visit this website");
else if (age >= 18 && age <= 22) {
    const answer = confirm("Are you sure you want to continue");
    if (answer == true) alert("Welcome, " + name);
    else alert("You are not allowed to visit this website");
} else if (age > 22) alert("Welcome, " + name);