import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import Alert from "../Alert/components/AlertApp/Alert"
import '../ProductList/ProductList.scss'
import Cart from "../Cart/Cart";

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            products: [],
            carts:[],
            active: false
        });

        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        fetch('../../Products.json')
            .then(
            item => item.json())
            .then(item=>
                this.setState({
                    products: item
                })
        );
        document.addEventListener('mousedown', this.handleClickOutside);
    }
    change = (e) => {
           this.setState({
               active: true
           });
        localStorage.removeItem('clickedProduct');
        console.log(localStorage.getItem('clickedProduct'));
        localStorage.setItem('clickedProduct',e.target.parentElement.parentElement.id);

    };

    okFunction = (e) => {
        this.setState({
            active: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
        let isContain = false;
        this.state.carts.forEach(item => {
            if(item.productNumber.toString() === localStorage.getItem('clickedProduct')){
                isContain = true;
            }
        });
        console.log(isContain);
        this.state.products.forEach(item => {
            if(item.productNumber.toString() === localStorage.getItem('clickedProduct') && !isContain){
                this.setState({
                    carts: [...this.state.carts,item]
                });
            }
            else {
            }
        });
    };
    cancelFunction = () => {
        this.setState({
            active: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    closeSpan = () => {
        this.setState({
            active: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                active: false,
                buttonActive: true
            });
            document.querySelector('.App').classList.toggle('active');
        }
    }

    clearCarts = () => {
        this.setState({
            carts:[]
        })
    };
    render() {
        const products = this.state.products.map((item,num) => <ProductCard key={num} onClickAlert={this.change} userInfo={item}/>);
        return (
            <>
                <div className="container">
            <div className={"productList"}>
                {products}
            </div>
                {  this.state.active ?
                    <Alert okFunction = {this.okFunction} cancelFunction={this.cancelFunction} closeSpan={this.closeSpan} firstModalActive={this.state.active}/>
                    : null
                }
                </div>
                <Cart cartList={this.state.carts} onclick={this.clearCarts}/>
                </>
        );
    }
}
export default ProductList;