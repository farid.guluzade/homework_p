import React, {Component} from 'react';
import './Cart.scss'

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeDiv : false
        }
    }

    showCarts = () => {
        this.setState({
        activeDiv: ! this.state.activeDiv
    })
    };
    render() {

        const cartProduct = this.props.cartList.map( item =>
            <p key={item.productNumber} className={"cartProduct-Info"}>{item.name}:{item.price}
            <img src={item.imgSrc} className={"cartProduct-Image"} alt={"product"}/>
            </p>);
        return (
            <>
            <button className={"cart-button"} onClick={this.showCarts}>
                <div className={"cart-buttonDiv"}>
                    <img className={"cart-buttonDivImage"} src="../images/cart@1X.png"  alt="cart icon"/>
                    <span className={"cart-buttonDivName"}>Cart</span>
                </div>
            </button>
                { this.state.activeDiv ?
            <div className={"cart-div"}>
                <button className={"cart-divButton"} onClick={this.props.onclick}>Clear Carts</button>
                {cartProduct}
            </div>
                    : null
                    }
                </>
        );
    }
}
export default Cart;