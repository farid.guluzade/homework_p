import React, {Component} from 'react';
import './App.scss';
import {BrowserRouter as Router, Link} from 'react-router-dom'
import {Switch, Route} from 'react-router'
import Cart from "./components/Cart/Cart";
import ProductList from "./components/ProductList/ProductList";
import Alert from "./components/Alert/components/AlertApp/Alert";
import Favorites from "./components/Favorites/Favorites";


class App extends Component {
    constructor(props) {
        super(props);
        if (JSON.parse(localStorage.getItem('Carts')) !== null && JSON.parse(localStorage.getItem('favorites'))!==null) {
            this.state = ({
                products: [],
                carts: JSON.parse(localStorage.getItem('Carts')),
                favorites:JSON.parse(localStorage.getItem('favorites')),
                active: false
            });
        } else if(JSON.parse(localStorage.getItem('Carts')) === null && JSON.parse(localStorage.getItem('favorites')) !==null){
            this.state = ({
                products: [],
                carts: [],
                favorites:JSON.parse(localStorage.getItem('favorites')),
                active: false
            });
        }
        else if(JSON.parse(localStorage.getItem('Carts')) !== null && JSON.parse(localStorage.getItem('favorites')) ===null)
        {
            this.state = ({
                products: [],
                carts: JSON.parse(localStorage.getItem('Carts')),
                favorites:[],
                active: false
            });
        }
        else {
            this.state = ({
                products: [],
                carts: [],
                favorites:[],
                active: false
            });
        }
    }

    okFunction = (e) => {
        this.setState({
            active: false
        });
        this.forceUpdate();
        document.querySelector('.App').classList.toggle('active');
        this.state.products.forEach(item => {
            if (item.productNumber.toString() === localStorage.getItem('clickedProduct')) {
                this.setState({
                    carts: this.state.carts.concat(item)
                });
            } else {
            }
        });
    };

    cancelFunction = () => {
        this.setState({
            active: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    closeSpan = () => {
        this.setState({
            active: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    changeFavorite = (e) => {
        // localStorage.removeItem('clickedFavorite');
        // localStorage.setItem('clickedFavorite', e.target.parentElement.id)
        const id = e.target.parentElement.parentElement.id;
        const favoriteList = this.state.favorites;
        let i = 0;
        favoriteList.forEach(item => {
            console.log(item.productNumber + " " + id);
            if (item.productNumber.toString() === id) {
                favoriteList.splice(i, 1);

            }
            i++;
            localStorage.setItem('favorites', JSON.stringify(favoriteList));
            console.log(this.state.carts);
            console.log(JSON.parse(localStorage.getItem('favorites')));
            this.setState({
                favorites: JSON.parse(localStorage.getItem('Carts'))
            });
        });
        i = 0;
        window.location.reload();
        this.forceUpdate();
    };

    changeCart = (e) => {
        const id = e.target.parentElement.parentElement.id;
        const cartList = this.state.carts;
        let i = 0;
        cartList.forEach(item => {
            console.log(item.productNumber + " " + id);
            if (item.productNumber.toString() === id) {
                cartList.splice(i, 1);

            }
            i++;
            localStorage.setItem('Carts', JSON.stringify(cartList));
            console.log(this.state.carts);
            console.log(JSON.parse(localStorage.getItem('Carts')));
            this.setState({
                carts: JSON.parse(localStorage.getItem('Carts'))
            });
        });
        i = 0;
        window.location.reload();
        this.forceUpdate();
    };

    onStarClick(nextValue, prevValue, name, event) {
        this.setState({rating: nextValue});
        localStorage.setItem('selectedFavorite',event.target.parentElement.parentElement.parentElement.parentElement.id);
    }

    onStarChange = (e) => {
        // this.forceUpdate();
        this.state.products.forEach(item => {
            if (item.productNumber.toString() === localStorage.getItem('selectedFavorite')) {
                console.log("yes-yes")
                this.setState({
                    favorites: this.state.favorites.concat(item)
                });
            } else {
            }
        });
        localStorage.setItem('favorites',JSON.stringify(this.state.favorites));
    };

    change = (e) => {
        this.setState({
            active: true
        });
        localStorage.removeItem('clickedProduct');
        localStorage.setItem('clickedProduct', e.target.parentElement.parentElement.id);
    };


    componentDidMount = () => {
        fetch('../../Products.json')
            .then(
                item => item.json())
            .then(item =>
                this.setState({
                    products: item
                })
            );
        document.addEventListener('mousedown', this.handleClickOutside);
    };

    render() {
        return (
            <Router>
                <div className="App">
                    <header className="menu">
                        <a href="https://www.fragrantica.com" className="site-logo">
                            <h1><span className="logoName">N-Parfumery</span></h1>
                        </a>
                        <div className="menu-links">
                            <div style={{marginRight: "20px"}}><Link
                                style={{color: "white", textDecoration: "none", fontSize: "25px"}} to={"/"}>Home</Link>
                            </div>
                            <div style={{marginRight: "20px"}}><Link
                                style={{color: "white", textDecoration: "none", fontSize: "25px"}}
                                to={"/Carts"}>Carts</Link></div>
                            <div style={{marginRight: "20px"}}><Link
                                style={{color: "white", textDecoration: "none", fontSize: "25px"}} to={"/Favorites"}>My
                                favorites</Link></div>
                        </div>
                    </header>
                    {this.state.active ?
                        <Alert okFunction={this.okFunction} cancelFunction={this.cancelFunction}
                               closeSpan={this.closeSpan} firstModalActive={this.state.active}/>
                        : null
                    }
                    <Switch>
                        <Route exact path="/">
                            <ProductList productList={this.state.products} onChange={this.okFunction}
                                         active={this.state.active} onAdd={this.change} onStar={this.onStarClick}
                                         onStarChangeApp={this.onStarChange}/>
                        </Route>
                        <Route exact path="/Carts">
                            <Cart productList={this.state.carts} onclick={this.changeCart}/>
                        </Route>
                        <Route exact path="/Favorites">
                            <Favorites productLists={this.state.favorites} onclick={this.changeFavorite}/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        );

    }
}

export default App;
