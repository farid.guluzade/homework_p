import React, {Component} from 'react';
import './CartProductCard.scss'

class ProductCard extends Component {
    render() {
        return (
            <div id={this.props.userInfo.productNumber} key={this.props.userInfo.productNumber} className="card">
                <img className={"Product-Image"} src={this.props.userInfo.imgSrc} alt="Denim Jeans"/>
                <p className="Product-Name">{this.props.userInfo.name}</p>
                <p className="price">${this.props.userInfo.price}</p>
                <p><button onClick={this.props.onClickAlert} style={{background:"red",color:"white"}}>X</button></p>
            </div>
        )
    }
}
export default ProductCard;