const container = document.getElementById('filter-titles');
const img = document.querySelectorAll('.image');

container.addEventListener('click', (e) => {
    if (e.target.classList.contains('tabs-second-title')) {
        const title = e.target;
        const type = title.dataset.filterby || 'image';
        const active = title.classList.contains('active-second');
        if (!active) {
            document.querySelector('.tabs-second-title.active-second').classList.remove('active-second');
            title.classList.add('active-second');
            filterByClassName(img, type);
        }
        console.log(type);
    }
})

function filterByClassName(elements, className) {
    for (let element of elements) {
        element.hidden = !element.classList.contains(className);
    }
}

const btn = document.getElementById('load');
const loadImg = document.querySelectorAll('#show');

for (let element of loadImg) {
    element.style.display = 'none';
}

btn.addEventListener('click', (e) => {

    for (let elm of loadImg) {
        elm.style.display = 'flex';
    }

    btn.style.display = 'none';


});