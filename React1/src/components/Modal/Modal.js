import React, {Component} from 'react';
import '../../components/Modal/Modal.scss'
import PropTypes from 'prop-types'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            header: this.props.header,
            closeButton : this.props.closeButton,
            text: this.props.text,
            actions : this.props.actions
        };
    }
    render() {
        return (
            <div className="modalStyle" >
                <div className="modalHeaderContent">
                    <h1 className="modalHeaderText">{this.state.header}</h1>
                    {
                        this.state.closeButton ?
                        <span className="close" onClick={this.props.closeSpan}></span> : null}
                </div>
                <div className="modalContent">
                    <div className="modalContentText">
                        <p className="modalContext">{this.state.text}</p>
                    </div>
                    <div className="modalButtons">
                        {this.state.actions}
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.arrayOf(Component)
};

export default Modal;