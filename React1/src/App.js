import React, { Component} from 'react';
import './App.css';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstModal: false,
            secondModal: false,
            buttonActive: true
        };

        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    okFunction = () => {
        this.setState({
            firstModal: false,
            secondModal: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    firstModalFunction = () => {
        this.setState({
            firstModal: true,
            buttonActive: false
        });
        document.querySelector('.App').classList.toggle('active');
    };
    secondModalFunction = () => {
        this.setState({
            secondModal: true,
            buttonActive: false
        });
        document.querySelector('.App').classList.toggle('active');
    };
    cancelFunction = () => {
        this.setState({
            firstModal: false,
            secondModal: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    closeSpan = () => {
        this.setState({
            firstModal: false,
            secondModal: false,
            buttonActive: true
        });
        document.querySelector('.App').classList.toggle('active');
    };

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                firstModal: false,
                secondModal: false,
                buttonActive: true
            });
            document.querySelector('.App').classList.toggle('active');
        }
    }

    render() {
        return (
            <div className="App">
                {this.state.firstModal ?
                    <div  ref={this.setWrapperRef} className="modalContainer">
                        <Modal header="Do you want to delete this file?" closeButton={true} text="Once you delete this file, it won’t be possible to undo this action.
            Are you sure you want to delete it?" closeSpan={this.closeSpan}
                               actions={[<Button key="1" backgroundColor="#b3382c" text="Ok" onclick={this.okFunction}/>,
                                   <Button key="2" backgroundColor="#b3382c" text="Cancel" onclick={this.cancelFunction}/>]}
                        /></div> : null
                }
                {this.state.secondModal ?
                    <div  ref={this.setWrapperRef} className="modalContainer">
                        <Modal  header="Do you want to save this file?" closeButton={false} text="Once you save this file, it won’t be possible to undo this action.
Are you sure you want to save it?" closeSpan={this.closeSpan}
                                actions={[<Button key="1" backgroundColor="#b3382c" text="Ok" onclick={this.okFunction}/>,
                                    <Button key="2" backgroundColor="#b3382c" text="Cancel" onclick={this.cancelFunction}/>]}
                        /></div> : null
                }
                {
                    this.state.buttonActive ?
                        <Button backgroundColor="red" text="Open first modal" onclick={this.firstModalFunction}/> : null
                }
                {
                    this.state.buttonActive ? <Button key="4" backgroundColor="blue" text="Open second modal"
                                                      onclick={this.secondModalFunction}/> : null
                }
            </div>
        );
    }
}
export default App;
